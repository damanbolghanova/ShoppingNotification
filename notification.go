package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"n_notify/greet"
	"net"
	"net/smtp"
)

type Server struct {
	greet.UnimplementedNotificationServiceServer
}

func (s *Server) SendEmailAdd(ctx context.Context, req *greet.MessageRequest) (*greet.MessageResponse, error) {
	fmt.Printf("Notification was invoked with %v \n", req)
	email_toSend := req.GetUserEmail()
	//fmt.Println(email_toSend)

	// Choose auth method and set it up
	auth := smtp.PlainAuth("", "amanbolganovadaria@gmail.com", "doripass568wor", "smtp.gmail.com")

	// Here we do it all: connect to our server, set up a message and send it
	to := []string{email_toSend}
	msg := []byte("To: " + email_toSend + "\r\n" +
		"Subject: Product was added\r\n" +
		"\r\n" +
		"Here’s the space for our great sales pitch\r\n")
	err := smtp.SendMail("smtp.gmail.com:587", auth, "amanbolganovadaria@gmail.com", to, msg)
	if err != nil {
		log.Fatal(err)
	}

	res := greet.MessageResponse{Done: true}
	return &res, nil
}

func (s *Server) SendEmailDel(ctx context.Context, req *greet.MessageRequest) (*greet.MessageResponse, error) {
	fmt.Printf("Notification was invoked with %v \n", req)
	email_toSendDel := req.GetUserEmail()
	//fmt.Println(email_toSend)

	// Choose auth method and set it up
	auth := smtp.PlainAuth("", "amanbolganovadaria@gmail.com", "doripass568word", "smtp.gmail.com")

	// Here we do it all: connect to our server, set up a message and send it
	to := []string{email_toSendDel}
	msg := []byte("To: " + email_toSendDel + "\r\n" +
		"Subject: Product was deleted\r\n" +
		"\r\n")
	err := smtp.SendMail("smtp.gmail.com:587", auth, "amanbolganovadaria@gmail.com", to, msg)
	if err != nil {
		log.Fatal(err)
	}

	res := greet.MessageResponse{Done: true}
	return &res, nil
}

func main() {

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greet.RegisterNotificationServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}

}
