FROM golang
ADD . /usr/src/notify
WORKDIR /usr/src/notify/
CMD ["go", "run", "."]